package co.lujun.testremoteservice;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity {

    private ITestRemoteService remoteService;//定义接口变量
    private boolean isStarted = false;
    private CounterServiceConnection conn;//定义连接变量，实现ServiceConnection接口


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnStart = (Button) findViewById(R.id.btn_start);
        Button btnStop = (Button) findViewById(R.id.btn_stop);
        Button btnBind = (Button) findViewById(R.id.btn_bind);
        Button btnRelease = (Button) findViewById(R.id.btn_release);
        Button btnInvote = (Button) findViewById(R.id.btn_invote);

        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onStartSerrvice();
            }
        });

        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onStopService();
            }
        });

        btnBind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBindService();
            }
        });

        btnRelease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onReleaseService();
            }
        });

        btnInvote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onInvoteService();
            }
        });
    }

    private void onStartSerrvice(){
        Intent intent = new Intent();
        intent.setClassName("co.lujun.testremoteservice", "co.lujun.testremoteservice.TestRemoteService");
        startService(intent);
        isStarted = true;
        updateServiceStatus();
    }

    private void onStopService(){
        Intent intent = new Intent();
        intent.setClassName("co.lujun.testremoteservice", "co.lujun.testremoteservice.TestRemoteService");
        stopService(intent);
        isStarted = false;
        updateServiceStatus();
    }

    private void onBindService(){
        if (conn == null){
            conn = new CounterServiceConnection();
            Intent intent = new Intent();
            intent.setClassName("co.lujun.testremoteservice", "co.lujun.testremoteservice.TestRemoteService");
            bindService(intent, conn, BIND_AUTO_CREATE);
            updateServiceStatus();
        }
    }

    private void onReleaseService(){
        if (conn != null){
            unbindService(conn);
            conn = null;
            updateServiceStatus();
        }
    }

    private void onInvoteService(){
        if (conn != null){
            try{
                Integer counter = remoteService.getCounter();//一旦client成功绑定到Service，就可以直接使用stub中的方法。
                TextView tvCounter = (TextView) findViewById(R.id.tv_counter);
                tvCounter.setText("Counter value:" + Integer.toString(counter));
            }catch (RemoteException e){
                Log.e(getClass().getSimpleName(), e.toString());
                e.printStackTrace();
            }
        }
    }

    private class CounterServiceConnection implements ServiceConnection{

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            // 从连接中获得stub对象，根据我们的跟踪，remoteService就是service中的stub对象
            remoteService = ITestRemoteService.Stub.asInterface(service);
            showInfo("onServiceConnected()" + remoteService);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            remoteService = null;
            showInfo("onServiceDisconnected()");
        }
    }

    private void updateServiceStatus(){
        TextView tvStatus = (TextView) findViewById(R.id.tv_status);
        tvStatus.setText("Service status:" + (conn == null ? "ubound" : "bound") + ";"
                + (isStarted ? "started" : "not started"));
    }

    //跟踪信息
    private void showInfo(String s){
        System.out.println("[" + getClass().getSimpleName() + "@" + Thread.currentThread().getName() + "]" + s);
    }
}
