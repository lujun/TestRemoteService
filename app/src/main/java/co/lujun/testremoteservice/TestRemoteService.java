package co.lujun.testremoteservice;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;

/**
 * Created by Administrator on 2015/7/10.
 */
public class TestRemoteService extends Service {
    private Handler serviceHandler;
    private int counter = 0;
    private TestCounterTask myTask = new TestCounterTask();

    @Override
    public void onCreate() {
        super.onCreate();
        serviceHandler = new Handler();
        serviceHandler.postDelayed(myTask, 1000);
        showInfo("remote service onCreate()");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        serviceHandler.removeCallbacks(myTask);
        serviceHandler = null;
        showInfo("remote service onDestory()");
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        showInfo("remote service onStart()");
    }

    //具体实现接口中暴露给client的Stub，提供一个stub inner class来具体实现。
    private ITestRemoteService.Stub stub = new ITestRemoteService.Stub(){

        @Override
        public void basicTypes(int anInt, long aLong, boolean aBoolean,
                               float aFloat, double aDouble, String aString) throws RemoteException {
            showInfo("basicTypes()");
        }

        //具体实现AIDL文件中接口的定义的各个方法
        @Override
        public int getCounter() throws RemoteException {
            showInfo("getCounter()");
            return counter;
        }
    };

    //当client连接时，将触发onBind()，Service向client返回一个stub对象，由此client可以通过stub对象来访问Service，
    // 本例中通过stub.getCounter()就可以获得计时器的当前计数。在这个例子中，我们向所有的client传递同一stub对象。
    @Override
    public IBinder onBind(Intent intent) {
        showInfo("remote service onBind()" + stub); //我们特别跟踪了stub对象的地址，可以在client连接service中看看通过ServiceConnection传递给clien
        return stub;
    }

    private class TestCounterTask implements Runnable{

        @Override
        public void run() {
            ++ counter;
            serviceHandler.postDelayed(myTask, 1000);
            showInfo("running" + counter);
        }
    }

    //跟踪信息
    private void showInfo(String s){
        System.out.println("[" + getClass().getSimpleName() + "@" + Thread.currentThread().getName() + "]" + s);
    }
}
